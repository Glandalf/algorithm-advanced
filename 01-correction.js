// Je peux appeler une fonction AVANT de l'avoir déclaré grâce au hoisting
// wrapWords(getWordsList(), '-')

// On teste ici si le mot zefzefzef appartient au dictionnaire
// console.log(isAWord('loin', getWordsList()))

// On récupère la liste des trigrammes d'un mot donné
//console.log(getTrigrams('#pouet#'))

const debut = Date.now()

const  text = 'Le petit apalace est dans la boite'
const trigramsTable = buildTrigramsIndex(wrapWords(getWordsList()))
const dico = getWordsList()

for (let word of text.split(' ')) {
    if (!isAWord(word.toLowerCase(), dico)) {
        const matchingWords = getWordsSharingTrigrams(word, trigramsTable)
        console.log(word)
        console.log(sortMatchingWords(matchingWords))
        // console.log(`--- Suggestion pour ${word} ---`, sortMatchingWords(getWordsSharingTrigrams(word.toLowerCase(), trigramsTable)))
    }
}
// const trigramsTable = buildTrigramsIndex(wrapWords(getWordsList()), '01-trigrams-table.dat')
const matchingWords = getWordsSharingTrigrams('patate', trigramsTable)
// console.log(sortMatchingWords(matchingWords))

const fin = Date.now()
console.log(fin - debut)

function getWordsList(fileName='./liste.de.mots.francais.frgut.txt') {
    /**
     * Cette fonction prend en entrée un nom de fichier textuel 
     * contenant un mot par ligne, en extrait tous les mots
     * et les retourne sous forme d'un tableau.
     * 
     * Cette fonction "semble" pure
     * Pourtant, si je ne change pas le nom du fichier que je lui passe
     * mais que je change le contenu du fichier, le résultat changera.
     */
    const fs = require('fs')
    return fs.readFileSync(fileName, 'utf8').split('\n')
}


function isAWord(word, dictionary) {
    /**
     * Cette fonction prend en paramètre un mot et une liste de mots
     * Elle retourne vrai si le mot se trouve dans la liste, faux sinon
     * 
     * Complexité : 0(n) dans le pire des cas
     *      Mais si je ne mets pas de break, je suis en O(n) dans TOUS les cas
     *      Alors que si je break dès que je trouve, j'ai une complexité moyenne bien meilleure (O(n/2) environ)
     *      qui reste dans la même classe de complexité mais qui est indiscutablement meilleur.
     */
    let found = false
    dictionary.forEach(realWord => {
        if (word === realWord) {
            found = true;
            // break;
        }
    });
    return found
}


function wrapWord(word, wrapper="#") {
    // Complexité : 0(1) temps constant
    // Fonction pure !
    return `${wrapper}${word}${wrapper}`
    // Même algo mais moins performant car 
    // c'est un autre opérateur de concaténation qui
    // est appelé ici. Et il est moins rapide pour ça :
    // return wrapper + word + wrapper
}

function wrapWords(wordsList, wrapper='#') {
    /**
     * Cette fonction prend en entrée une liste de mots ainsi qu'un caractère spécial
     * Pour chaque mot du dictionnaire, elle ajoute le caractère spécail en début et fin du mot
     * 
     * Complexité : 0(n)
     * 
     * Une fonction, pour être pure doit :
     * - ne modifier aucun élément passé en paramètre ou partagé par le reste du programme ou du système
     * - avoir systématiquement le même résultat pour les mêmes entrées.
     * 
     * Nous avons 2 cas. Dans le premier, nous n'avons pas une pure function puisqu'on modifie 
     * un paramètre d'entrée. Dans le deuxième ce n'est pas le cas, ce qui évite "tout" effet de bord
     * d'une part et la fonction aura toujours le même résultat pour les mêmes paramètres.
     */
    // Cas n°1: on modifie directement le dictionnaire passé en param
    // for (let i=0; i<wordsList.length ; i++) {
    //     wordsList[i] = wrapWord(wordsList[i], wrapper)
    // }
    // Cas n°2: on ne touche pas au param, on crée un nouveau dico
    const newDico = []
    for (let word of wordsList) {
        newDico.push(wrapWord(word, wrapper))
    }

    // Dans le cas 2 on ne touche qu'à notre var déclarée localement :
    // console.log(wordsList[10])
    // console.log(newDico[10])
    return newDico
}


function getTrigrams(word) {
    /**
     * Complexité : O(n)
     * Pure function: oui
     * 
     * Cette fonction prend en entrée un mot et affiche/retourne
     * la liste des trigrammes qui le composent.
     * 
     * On sait qu'un mot fera obligatoirement au moins 3 caractères => au moins 1 trigramme
     */
    // Ce test n'est pas nécessaire du fait de notre algo, mais ça reste propre
    if (word.length < 3) {
        return []
    }
    const trigrams = []
    // On doit faire attention à s'arrêter au bon moment
    for (let i=0 ; i < word.length-2 ; i++) {
        // FIXME: il est TRES PROBABLE que l'accès par pointeur pose souci avec les caractères accentués
        trigrams.push(`${word[i]}${word[i+1]}${word[i+2]}`)
    }
    return trigrams
}


function buildTrigramsIndex(dictionnary, outputFileName) {
    /** 
     * Complexité : O(n²) environ (2 * n²) si on considère qu'itérer sur les trigrammes est significatif, sinon O(n² * n) environ
     * Pure function : on écrit dans un fichier, donc moyen.
     * 
     * Cette fonction prend en entrée un dictionnaire.
     * Elle calcule tous les trigrammes de tous les mots et 
     * et les ajoute dans une liste référencançant tous les trigrammes 
     * ainsi que les mots qui le contiennent.
     */
    const fs = require('fs')
    const trigramsTable = {}

    for (let i=0 ; i<dictionnary.length ; i++) {
        const currentWord = dictionnary[i]
        // On extrait tous les trigrammes
        const trigrams = getTrigrams(currentWord)

        // Pour chaque trigramme :
        for (let trigram of trigrams) {
            //   On vérifie s'il est déjà dans notre trigramsTable
            //     - si oui, on ajoute le mot à sa liste
            //     - si non, on crée la clé et on ajoute le mot à sa liste
            if (!trigramsTable[trigram]) {
                trigramsTable[trigram] = []
            }
            trigramsTable[trigram].push(currentWord)
        }
    }

    // Transformer notre objet trigramTables en une chaine de caractères qu'on stocker nianiania
    if (outputFileName) {
        const trigramsTableDump = dumpAsPlainText(trigramsTable)
        // sauvegarder dans le fichier dédié
        try {
            const data = fs.writeFileSync(outputFileName, trigramsTableDump)
            //file written successfully
        } catch (err) {
            console.error(err)
        }
    }

    return trigramsTable
}

function dumpAsPlainText(trigramsTable) {
    /**
     * Complexité : Quelque part entre O(n) et O(n²) (parce que le join se fait sur un tableau à l'intérieur du for)
     * Pure function : 
     */
    let output = ''
    for (const [key, value] of Object.entries(trigramsTable)) {
        output += `${key} : ${value.join(',').replace(/#/g, '')}\n`
    }
    return output
}

function getWordsSharingTrigrams(word, trigramsTable) {
    /**
     * Complexité : nb trigrammes * nb mot pour un trigramme => ??? O(n)
     * Pure function : oui
     * 
     * Cette fonction prend un mot en entrée et retourne la liste
     * de tous les mots contenants les trigrammes du mot en cours :
     * un mot apparati autant de fois qu'il partage de trigrammes 
     * avec le mot passé en paramètre.
     */
    const matchingWord = []
    for (let trigram of getTrigrams(word)) {
        // récupérer tous les mots dans ma table de correspondance
        matchingWord.push(...trigramsTable[trigram])
    }
    return matchingWord
}

function sortMatchingWords(wordsSharingTrigrams, max=10) {
    /**
     * Cette fonction prend en paramètre une liste de mots *avec doublon*
     * et retourne les `max` premiers mots par nombre d'occurences décroissant
     */
    const counts = {}
    for (let word of wordsSharingTrigrams) {
        if (!counts[word]) {
            counts[word] = 1
        }
        else {
            counts[word]++
        }
    }

    const output = []
    for (let [key, value] of Object.entries(counts)) {
        if (value > 0) {
            output.push([key, value])
        } 
    }

    
    output.sort((a,b) => b[1] - a[1])
    return output.slice(0, max)
}
