


# Pull request validation on develop branch

Cette pipeline sert à vérifier que les modifications apportées sur la branche qui doit être fusionnée sont conformes aux attentes, qu'il n'y a pas de régression dans les fonctionnalités et qu'il n'y a pas de faille connue dans les codes aussi bien côté back que front.

```bash
shopt -s globstar; pylint api/**/*.py
tslint 'vue/src/**/*.ts?(x)'
```

## Triggering conditions

| key               | value                | Description                                                      |
|:------------------|:---------------------|:-----------------------------------------------------------------|
| gitEvent          | pull-request         | other values are `branch` `commit` `push` `pull-request` `merge` |
| sourceBranch      | *                    |                                                                  |
| destinationBranch | dev                  |                                                                  |
| author            | \*.\*@asciiparait.fr |                                                                  |
| reviewer          |                      |                                                                  |


## Pipeline steps

### Merge source and destination branches locally

```bash
if [ ! -e ./OWeb/.git ]
then
    git clone git@ssh.dev.azure.com:v3/som-methodes/OWeb/OWeb
else
    git stash
    git checkout ${sourceBranch}
fi
git pull ${remoteBranch}
echo "zefoihzeiofuhzifhu"
```


### Code audit

```bash
shopt -s globstar; pylint api/**/*.py
tslint 'vue/src/**/*.ts?(x)'
```


### Vue end to end tests

```bash
cd vue
npm install
npm run build
npm run:test
```

### Build Vue and Flask files

```bash
docker run -v ./vue:/home/app ... vue-compiler
docker build -t flask-server --build-arg SOURCE_FOLDER=./api -c ${DOCKERFILES_PATH}/python-flask.Dockerfile .
```


### Copy binaries & start apps

```bash
copy ./api /home/ci-scuti/environments/dev/ci-sctui/api
copy ./vue/dist /home/ci-scuti/environments/dev/ci-sctui/app
docker stack deploy -c ${DOCKERSTACKS_PATH}/ci-scuti ci-scuty
```


### API Integration tests

```bash
cd api
pipenv run ./tests/integration/test-api.py
```

### VCS feedback

```bash
curl https://dev.azure.com/jkufehgisduhgidksfhgbvkxsdfjhbkxsidfhbvwukdjfhbv 
```