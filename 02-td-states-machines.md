# Introduction aux machines à état

Les automates finis sont des types d'algorithmes très utilisés dans différents domaines de l'algorithmique et notamment dans celui de l'analyse syntaxique.

Nous allons justement prendre le cas pratique d'un analyseur de fichier au format `markdown` dans lequel on va vouloir extraire certaines parties bien particulières. L'idée est de rédiger des documentations de déploiement logiciel, étape par étape, contenant des blocs de code (format bash). Une fois ces documentations écrites, on aimerait avoir un moteur qui analyse ces documents et déploie automatiquement notre code en fonction. Voilà à quoi ressembleraient ces fichiers :


!!! note Comment feriez-vous ?

    Que faudrait-il faire pour arriver à terme à extraire tous les blocs de code en bash (qui seront exécutés plus tard mais ce n'est pas le sujet ici) ?


Un automate fini est aussi souvent appelé "Machine à état transition".

- Un ensemble d'états
- Un ensemble de transitions possible depuis un état donné


Dans un markdown on pourrait avoir :

- un état initial qui autorise 2 transitions :
  - une de type "ligne vide" -> état initial
  - une de type "titre de niveau 1" -> état "A" ou "j'ai rencontré mon tritre 1, j'espère ne plus jamais en voir"
  - toute autre transition amènerait une erreur d'exé
- un état "A"
  - une transition de type "ligne vide" -> "B" ou "j'attends n'importe quoi mais pas un <h1>"



Le but est de parcourir ligne par ligne le fichier et d'identifier toutes les lignes qui correspondent à du code de type bash dans la partie "Pipeline steps"


!!! note pour faire cela, je partairais peut-être sur...
    
    Je regarderais chaque début de ligne pour voir si on a un # (ou plus)
