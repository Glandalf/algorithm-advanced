# Notes du cours

## A faire

> Donner la liste des gens qui n'ont pas encore fait le rendu
Algo de la carte vitale ?
"L'algorithmique de l'autre côté du miroir" Aurélie JEAN

## Petit débrief

* les pres plus courtes (surtout le temps de débrief)
* Choix techno libre apprécié, étape par étape cool
* Morceau de code à trou ou pseudo code à trou pour un algo donné


## Présents

Axel CHUINE
Julien REVEST
Manon
Dylan
Quentin
Wided

Thomas retard de 5 min
Enzo 10 min

## Niveau actuel

Axel : peu de restes, mauvaise expérience
Dylan : quelques restes : pratique pseudo code + python, pas trop d'appréhension
Enzo : pratiqué pendant deux ans.
Julien : Pas trop de restes
Manon : Base très fragile (peu de cours)
Quentin : quelques réflexes sans doute mais il faudra consolider les concepts
Thomas FABRE : un peu de pratique en JAVA (sommes de salaires, etc.), sympa à niveau simple, craintes sur la difficulté de l'algo avancée
Wided : "L'algorithmique de l'autre côté du miroir" Aurélie JEAN

> Etre un peu ambitieux sur ce module car attentes
> path-finding, algos de tri, ia simples du style min-max

## Pres de Manon sur Quicksort

Présentation de deux implémentations, explication assez claire.
On a passé un peu de temps à décortiquer le but de cet algo et son fonctionnement.
On se pose la question : est-il utile de faire un algo si tordu pour trier un tableau ?! On a évoqué les problématiques d'obsolescence dues à des soucis d'optimisation.

## Thomas sur le jeu de la vie

Jeu créé par un mathématicien pour observer l'évolution d'une population de cellules. Le principe est simple, si une cellule est trop isolée, elle meurt, si elle est trop collée aux autres aussi, et si une "non cellule" est entourée du bon nombre de cellule, une nouvelle se crée. Puis on recommence.

> C'est quoi un canon ? C'est quoi un planneur, c'est quoi un canon à planneur ?!
On se rend compte que l'on peut créer un jeu de la vie dans le jeu de vie par exemple : l'algo permet la réflexivité notamment !!!! Tout dépend de notre grille de départ, sans avoir à toucher l'algo.

Ressemblance avec la fourmi de langhton, on pourrait se le coder d'ailleurs :).

## Quentin sur DIJKSTRA

Présentation interactive dans Paint : déroulé d'un exemple réel en live, sans filet.
Assez incroyable. On part d'un graphe connexe pondéré, on a un tableau des distances vide, l'algo est déroulé

## Enzo sur LEVENSHTEIN

Présentation de l'algo et déroulé sur un exemple : distance entre "chien" avec "chat".

## Wided avec DFS

Algo de parcours de graphes. Ressemble un peu à Dijkstra non ?
Son but est de trouver un chemin entre a et b : pas le plus court, juste un chemin.
son principe ressemble à celui de sortie d'un labirynthe : on essaie de parcourir le graphe en choisissant toujours l'élément le plus à gauche par exemple (en suivant tous)

## Dylan avec le crible d'Erathostène

On peut vérifier si un nombre est premier en essayant de le diviser par les nombres de 2 à racine de lui même. S'il n'est pas divisible, c'est qu'il est premier.

Le crible d'Atkin nous permet de trouver des nombres premiers dans l'ordre de leur apparition.

## Axel avec A*

Similaire à DIJKSTRA ou DFS : on parcours un graphe.
Une approche orientée sur la direction : si on veut aller dans une ville vers le nord, il va tenter directement d'aller vers le nord en éliminant les autres cas.
