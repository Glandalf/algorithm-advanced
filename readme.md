# Cours d'algoithmique avancée

Ceci est un cours très résumé de concepts essentiels de l'algorithmique.
Si vous souhaitez y voir apparaitre certains éléments, n'hésitez pas à m'en faire la demande.

## Partie 1 - Introduction par la pratique

Dans la première partie nous abordons un certain nombre de concepts de l'algorithmique par la pratique au travers d'un cas pratique très classique : un correcteur orthographique.

### Mise en pratique - correcteur orthographique

Nous souhaitons monter pas à pas un algorithme permettant d'analyser un mot ou un ensemble de mots et proposer des suggestions de correction si un mot n'apparait pas dans le dictionnaire.

Ce TD repose notamment sur une ressource externe mentionnée dans le support correspondant ([ce dictionnaire](https://chrplr.github.io/openlexicon/datasets-info/Liste-de-mots-francais-Gutenber) de mots français.)

Vous trouverez un support corrigé et commenté de cet exercice.

## Partie 2 - Complexité et automates finis

Dans cette seconde partie, on prend contact avec un autre type d'algorithme : les automates finis, ou machines à état transition dont l'exécution d'une instruction dépend d'un état acquis précédemment. 

Ces algorithmes dits séquentiels sont très utilisés, notamment dans l'analyse des langages formels (comme un langage de programmation).

### Mise en pratique - Parseur Markdown simplifié

Dans ce TD, en partant d'un fichier Markdown structuré selon un formalisme bien défini, on souhaite dans l'idéal produire un objet JSON représentant le fichier. Une version simplifiée de ce travail peut être d'extraire les morceaux de code spécifiquement liés à une partie du document (en vue de les exécuter plus tard par exemple).